<?php

namespace SpringGDS\Controllers;

use Plenty\Modules\Cloud\Storage\Models\StorageObject;
use Plenty\Modules\Order\Contracts\OrderRepositoryContract;
use Plenty\Modules\Order\Models\OrderItemType;
use Plenty\Modules\Order\Shipping\Countries\Contracts\CountryRepositoryContract;
use Plenty\Modules\Order\Shipping\Information\Contracts\ShippingInformationRepositoryContract;
use Plenty\Modules\Order\Shipping\Package\Contracts\OrderShippingPackageRepositoryContract;
use Plenty\Modules\Order\Shipping\PackageType\Contracts\ShippingPackageTypeRepositoryContract;
use Plenty\Modules\Plugin\Storage\Contracts\StorageRepositoryContract;
use Plenty\Plugin\Controller;
use Plenty\Plugin\Http\Request;
use Plenty\Plugin\ConfigRepository;
use Plenty\Modules\Order\Shipping\Package\Models\OrderShippingPackage;
use Plenty\Modules\Item\Item\Contracts\ItemRepositoryContract;
use Plenty\Modules\Item\Variation\Contracts\VariationRepositoryContract;
use Plenty\Modules\Item\VariationSku\Contracts\VariationSkuRepositoryContract;
use Plenty\Plugin\Log\Loggable;

/**
 * Class SpringGDSController
 * @package SpringGDS\Controllers
 */
class SpringGDSController extends Controller
{
    use Loggable;

    /** @var array $services */
    private $services = array(
        'TRCK' => 'Spring GDS TRACKED',
        'UNTR' => 'Spring GDS UNTRACKED',
        'SIGN' => 'Spring GDS SIGNATURE',
        'PPTT' => 'PostNL Packet Tracked',
        'PPTR' => 'PostNL Packet Registered',
        'PPBTT' => 'PostNL Packet Boxable Tracked',
        'PPBNT' => 'PostNL Packet Boxable Non Tracked',
        'PPBT' => 'PostNL Packet Bag & Trace',
    );

    /** @var array $items ( less foreach calls ) */
    private $items = array();

    /** @var string $currency */
    private $currency = 'EUR';

    /** @var int $totalPrice total order price */
    private $totalPrice = 0;

    /** @var OrderRepositoryContract $orderRepository */
    private $orderRepository;

    /** @var OrderShippingPackageRepositoryContract $orderShippingPackage */
    private $orderShippingPackage;

    /** @var ShippingInformationRepositoryContract $shippingInformationRepositoryContract */
    private $shippingInformationRepositoryContract;

    /** @var StorageRepositoryContract $storageRepository */
    private $storageRepository;

    /** @var ShippingPackageTypeRepositoryContract $shippingPackageTypeRepositoryContract */
    private $shippingPackageTypeRepositoryContract;

    /** @var  array $createOrderResult */
    private $createOrderResult = [];

    /** @var ConfigRepository $config */
    private $config;

    /** @var ItemRepositoryContract $itemRepositoryContract */
    private $itemRepositoryContract;

    /** @var VariationRepositoryContract $variationRepositoryContract */
    private $variationRepositoryContract;

    /** @var CountryRepositoryContract $countryRepositoryContract */
    private $countryRepositoryContract;

    /** @var VariationSkuRepositoryContract $variationSkuRepositoryContract */
    private $variationSkuRepositoryContract;

    /**
     * SpringGDSController constructor.
     * @param OrderRepositoryContract $orderRepository
     * @param OrderShippingPackageRepositoryContract $orderShippingPackage
     * @param StorageRepositoryContract $storageRepository
     * @param ShippingInformationRepositoryContract $shippingInformationRepositoryContract
     * @param ShippingPackageTypeRepositoryContract $shippingPackageTypeRepositoryContract
     * @param ConfigRepository $config
     * @param ItemRepositoryContract $itemRepositoryContract
     * @param VariationRepositoryContract $variationRepositoryContract
     * @param CountryRepositoryContract $countryRepositoryContract
     * @param VariationSkuRepositoryContract $variationSkuRepositoryContract
     */
    public function __construct(OrderRepositoryContract                $orderRepository,
                                OrderShippingPackageRepositoryContract $orderShippingPackage,
                                StorageRepositoryContract              $storageRepository,
                                ShippingInformationRepositoryContract  $shippingInformationRepositoryContract,
                                ShippingPackageTypeRepositoryContract  $shippingPackageTypeRepositoryContract,
                                ConfigRepository                       $config,
                                ItemRepositoryContract                 $itemRepositoryContract,
                                VariationRepositoryContract            $variationRepositoryContract,
                                CountryRepositoryContract              $countryRepositoryContract,
                                VariationSkuRepositoryContract         $variationSkuRepositoryContract)
    {
        $this->orderRepository = $orderRepository;
        $this->orderShippingPackage = $orderShippingPackage;
        $this->storageRepository = $storageRepository;
        $this->shippingInformationRepositoryContract = $shippingInformationRepositoryContract;
        $this->shippingPackageTypeRepositoryContract = $shippingPackageTypeRepositoryContract;
        $this->config = $config;
        $this->itemRepositoryContract = $itemRepositoryContract;
        $this->variationRepositoryContract = $variationRepositoryContract;
        $this->countryRepositoryContract = $countryRepositoryContract;
        $this->variationSkuRepositoryContract = $variationSkuRepositoryContract;
    }

    /**
     * Registers shipment(s)
     * @param Request $request
     * @param array $orderIds
     * @return array
     */
    public function registerShipments(Request $request, array $orderIds = array()): array
    {
        $shipmentDate = date('Y-m-d');
        $orderIds = $this->getOpenOrderIds($this->getOrderIds($request, $orderIds));
        $orderBundleIDs = [];

        foreach ($orderIds as $orderId) {
            $this->currency = 'EUR';
            $service = '';
            $i = 0;
            $this->items = array();
            $exchangeRate = 1;
            /** @var \Plenty\Modules\Order\Models\Order $order */
            $order = $this->orderRepository->findOrderById($orderId);
            $packages = $this->orderShippingPackage->listOrderShippingPackages($order->id);
            /** @var \Plenty\Modules\Order\Shipping\Countries\Models\Country $country */
            $country = $this->countryRepositoryContract->getCountryById($order->deliveryAddress->countryId);
            $regionRelations = json_decode($this->config->get('SpringGDS.regionRelations', ''), true);

            if (!empty($regionRelations)) {
                $service = strtoupper($regionRelations[$order->shippingProfileId][$country->shippingDestinationId]);
            }

            if (empty($service)) {
                $service = 'TRCK';
            }

            foreach ($order->amounts as $amount) {
                if ($amount->currency !== $this->currency) {
                    $this->currency = $amount->currency;
                    $exchangeRate = $amount->exchangeRate;
                    break;
                }
            }

            foreach ($order->orderItems as $item) {
                if ($item->itemVariationId <= 0) {
                    continue;
                }

                $parentBundleID = null;
                $sku = '';
                $itemWeight = 0;
                $quantity = $item->quantity;
                /** @var \Plenty\Modules\Item\Variation\Models\Variation $varById */
                $varById = (array)$this->variationRepositoryContract->show($item->itemVariationId, array(), 'en');
                /** @var \Plenty\Modules\Item\Item\Models\Item $itemById */
                $itemById = $this->itemRepositoryContract->show($varById['itemId'], array(), 'en');
                /** @var \Plenty\Modules\Order\Shipping\Countries\Models\Country $producingCountry */
                $producingCountry = $this->countryRepositoryContract->getCountryById($itemById->producingCountryId);
                $skuArray = $this->variationSkuRepositoryContract->findByVariationId($item->itemVariationId);

                /* Collect bundle items */
                if ($item->typeId === OrderItemType::TYPE_ITEM_BUNDLE) {
                    $orderBundleIDs[$item->id] = $item->itemVariationId;
                }

                switch ($this->config->get('SpringGDS.itemTitle', 'name1')) {
                    case 'name1':
                        $description = $itemById->texts[0]->name1;
                        break;
                    case 'name2':
                        $description = $itemById->texts[0]->name2;
                        break;
                    case 'name3':
                        $description = $itemById->texts[0]->name3;
                        break;
                    case 'shortDescription':
                        $description = $itemById->texts[0]->shortDescription;
                        break;
                    default:
                        $description = $item->itemName;
                }

                foreach ($skuArray as $skuArrayCell) {
                    if (!empty($skuArrayCell->sku)) {
                        $sku = $skuArrayCell->sku;
                        break;
                    }
                }

                foreach ($item->references as $itemReference) {
                    if ($itemReference->referenceType === 'bundle') {
                        $parentBundleID = $orderBundleIDs[$itemReference->referenceOrderItemId];
                        $quantity = $item->quantity - $this->items[$parentBundleID]['quantity'];
                    }
                }

                foreach ($item->properties as $property) {
                    if ($property->typeId === 11) { // item weight edited in order
                        $itemWeight = (int)trim($property->value) / 1000;
                    }
                }

                switch (true) {
                    case $itemWeight > 0:
                        break;
                    case $varById['weightG'] > 0:
                        $itemWeight = $varById['weightG'] / 1000;
                        break;
                    case $varById['weightNetG'] > 0:
                        $itemWeight = $varById['weightNetG'] / 1000;
                        break;
                }

                $price = $item->amounts[0]->priceOriginalNet *
                    ($item->amounts[0]->currency === 'EUR'
                        ? $exchangeRate
                        : $item->amounts[0]->exchangeRate) *
                    $quantity;

                if ($price > 0) {
                    $this->totalPrice += $price;
                    $this->items[] = array(
                        'Description' => $description ?? $item->orderItemName,
                        'Sku' => $sku ?? $varById['model'],
                        'HsCode' => (!empty($varById['customsTariffNumber'])
                            ? $varById['customsTariffNumber']
                            : $this->config->get('SpringGDS.hsCode')
                        ),
                        'OriginCountry' => $producingCountry->isoCode2,
                        'PurchaseUrl' => '',
                        'Quantity' => $quantity,
                        'Value' => number_format($price - 0.0001, 2, '.', ''),
                        'Weight' => $itemWeight > 0
                            ? $quantity * $itemWeight
                            : null,
                    );
                }
            }

            foreach ($packages as $package) {
                $shipmentItems = null;

                $responseApi = $this->curlApi(
                    $this->genData($package, $order, $service, ++$i)
                );

                if (!empty($responseApi->Error) && $responseApi->ErrorLevel > 0) {
                    // Reset order -> no handle -> API has status 'open', gives error and TrackingNumber is null
                    $this->createOrderResult[$orderId] = [
                        'success' => false,
                        'message' => 'Order # ' . $order->id . ', package ' . str_pad($i, 2, '0', STR_PAD_LEFT)
                            . '. While shipment register, API returned error #' . $responseApi->ErrorLevel . ': '
                            . $responseApi->Error,
                        'newPackagenumber' => false,
                        'packages' => $shipmentItems,
                    ];

                    continue;
                }

                $responseApi = $this->curlApi(
                    json_encode(array(
                        'Apikey' => $this->config->get('SpringGDS.apiKey'),
                        'Command' => 'GetShipmentLabel',
                        'Shipment' => array(
                            'LabelFormat' => $this->config->get('SpringGDS.format', 'PDF'),
                            'ShipperReference' => $order->plentyId . '-' . $order->id . '-'
                                . str_pad($i, 2, '0', STR_PAD_LEFT),
                        ),
                    ))
                );

                if (!empty($responseApi->Error) && $responseApi->ErrorLevel > 0) {
                    $this->createOrderResult[$orderId] = [
                        'success' => false,
                        'message' => 'Order # ' . $order->id . ', package ' . str_pad($i, 2, '0', STR_PAD_LEFT)
                            . '. While shipment register, API returned error #' . $responseApi->ErrorLevel . ': '
                            . $responseApi->Error,
                        'newPackagenumber' => false,
                        'packages' => $shipmentItems,
                    ];

                    continue;
                }

                // shipping service providers API should be used here
                $response = [
                    'labelUrl' => $this->genLabelUrl($responseApi->Shipment->TrackingNumber),
                    'shipmentNumber' => $responseApi->Shipment->TrackingNumber,
                    'sequenceNumber' => 1,
                    'status' => 'shipment successfully registered as ' . $this->services[$service],
                ];

                // handles the response
                $shipmentItems = $this->handleAfterRegisterShipment(
                    $response['labelUrl'],
                    $response['shipmentNumber'],
                    $package->id
                );

                // adds result
                $this->createOrderResult[$orderId] = [
                    'success' => true,
                    'message' => 'Shipment successfully registered as ' . $this->services[$service],
                    'newPackagenumber' => false,
                    'packages' => $shipmentItems,
                ];

                // saves shipping information
                $this->saveShippingInformation($orderId, $shipmentDate, $shipmentItems);
            }
        }

        return $this->createOrderResult;
    }

    /**
     * Cancels registered shipment(s)
     *
     * @param Request $request
     * @param array $orderIds
     * @return array
     */
    public function deleteShipments(Request $request, $orderIds): array
    {
        $orderIds = $this->getOrderIds($request, $orderIds);

        foreach ($orderIds as $orderId) {
            $error = '';
            $shippingInformation = $this->shippingInformationRepositoryContract->getShippingInformationByOrderId($orderId);
            $packages = $this->orderShippingPackage->listOrderShippingPackages($orderId);

            try {
                foreach ($packages as $package) {
                    // use the shipping service provider's API here
                    $response = $this->curlApi(
                        json_encode(
                            array(
                                'Apikey' => $this->config->get('SpringGDS.apiKey'),
                                'Command' => 'VoidShipment',
                                'Shipment' => array(
                                    'TrackingNumber' => $package->packageNumber, // or ShipperReference
                                ),
                            )
                        )
                    );

                    if (!empty($response->Error) || $response->ErrorLevel > 0) {
                        $error .= 'Code: ' . $response->ErrorLevel . ': ' . $response->Error . '<br>';
                    }
                }

                $this->createOrderResult[$orderId] = [
                    'success' => empty($error),
                    'message' => (empty($error) ? 'Shipment canceled' : $error),
                    'newPackagenumber' => false,
                    'packages' => null,
                ];

                if (isset($shippingInformation->additionalData) && is_array($shippingInformation->additionalData)) {
                    // resets the shipping information of current order in PlentyMarkets
                    $this->shippingInformationRepositoryContract->resetShippingInformation($orderId);
                }
            } catch (\Exception $e) {
                // exception handling
                $this->createOrderResult[$orderId] = [
                    'success' => false,
                    'message' => $e->getMessage(),
                    'newPackagenumber' => false,
                    'packages' => null,
                ];
            }
        }

        // return result array
        return $this->createOrderResult;
    }

    /**
     * @param Request $request
     * @param array $orderIds
     * @return array
     */
    public function getLabels(Request $request, $orderIds = array()): array
    {
        $orderIds = $this->getOrderIds($request, $orderIds);
        $labels = array();

        foreach ($orderIds as $orderId) {
            /** @var OrderShippingPackage $packages */
            $packages = $this->orderShippingPackage->listOrderShippingPackages($orderId);

            foreach ($packages as $package) {
                $labelKey = explode('/', $package->labelPath);
                $labelKey = array_pop($labelKey);

                if ($this->storageRepository->doesObjectExist('SpringGDS', $labelKey)) {
                    $storageObject = $this->storageRepository->getObject('SpringGDS', $labelKey);
                    $labels[] = $storageObject->body;
                }
            }
        }

        return $labels;
    }

    /**
     * @param object $package
     * @param object $order
     * @param string $service
     * @param int $i
     * @param string $type
     * @return string
     */
    protected function genData(object $package, object $order, string $service, int $i, string $type = ''): string
    {
        /* $package->packageId - package TYPE id >_< */
        $packageType = $this->shippingPackageTypeRepositoryContract->findShippingPackageTypeById($package->packageId);
        $extraConsignorAddress = [];
        $clearance = false;
        $ioss = '';
        $clearanceRelations = json_decode($this->config->get('SpringGDS.clearanceRelations', ''), true);
        $iossRelations = json_decode($this->config->get('SpringGDS.iossRelations'), true);
        $shippingDestinationId = $this->countryRepositoryContract
            ->getCountryById($order->deliveryAddress->countryId)
            ->shippingDestinationId;

        if (!empty($clearanceRelations)) {
            $clearance = $clearanceRelations[$order->shippingProfileId][$shippingDestinationId];
        }

        if (!empty($iossRelations)) {
            $ioss = $iossRelations[$order->shippingProfileId][$shippingDestinationId];
        }

        switch ($packageType->unit) {
            case 'INH':
                $dimRate = 0.3937007874015748; // = 1 / 2.54 [ inch = 2.54cm ]
                break;
            case 'MTR':
                $dimRate = 100;
                break;
            case 'MMT':
                $dimRate = 0.1;
                break;
            default:
                $dimRate = 1;
        }

        $name = $this->concatNotEmpty(array(
            $order->deliveryAddress->firstName,
            $order->deliveryAddress->lastName,
            $order->deliveryAddress->contactPerson,
        ));

        $additionalAddress = $this->concatNotEmpty(array(
            $order->deliveryAddress->address3,
            $order->deliveryAddress->address4,
            $order->deliveryAddress->postident,
        ));

        if ($clearance) {
            /*
• Vat is value prodived under VAT // Налог // canceled
• Eori is value prodived under EORI // ИНН // canceled
• NlVat is value prodived under Dutch VAT // Налог // canceled
• EuEori is value prodived under EU EORI // ИНН // canceled
• Ioss is value prodived under IOSS
             */
            $extraConsignorAddress = [
                'Ioss' => $ioss,
            ];
        }

        return json_encode(
            array(
                'Apikey' => $this->config->get('SpringGDS.apiKey'),
                'Command' => 'OrderShipment',
                'Shipment' => array(
                    'LabelFormat' => $this->config->get('SpringGDS.format', 'PDF'),
                    'ShipperReference' => $order->plentyId . '-' . $order->id . '-'
                        . str_pad($i, 2, '0', STR_PAD_LEFT)
                        . $type, // unique + R ($type) for return
                    'OrderReference' => $order->id,
                    'DisplayId' => ($this->config->get('SpringGDS.displayOrderId', 0) ? $order->id : ''),
                    'Service' => $service,
                    'CustomsDuty' => 'DD' . ($clearance ? 'P' : 'U'),
                    'ConsignorAddress' => array_merge(
                        array(
                            'Name' => $this->config->get('SpringGDS.senderName'),
                            'Company' => $this->config->get('SpringGDS.companyName'),
                            'AddressLine1' => $this->config->get('SpringGDS.senderStreet') . ' '
                                . $this->config->get('SpringGDS.senderNo'),
                            'AddressLine2' => '',
                            'City' => $this->config->get('SpringGDS.senderTown'),
                            'State' => $this->config->get('SpringGDS.senderState'),
                            'Zip' => $this->config->get('SpringGDS.senderPostalCode'),
                            'Country' => $this->config->get('SpringGDS.senderCountryIso'), // iso
                            'Phone' => $this->config->get('SpringGDS.senderPhone'),
                            'Email' => $this->config->get('SpringGDS.senderEmail'),
                        ),
                        $extraConsignorAddress
                    ),
                    'ConsigneeAddress' => array(
                        'Name' => $name,
                        'Company' => $order->deliveryAddress->companyName,
                        'AddressLine1' => $order->deliveryAddress->address1,
                        'AddressLine2' => $order->deliveryAddress->address2,
                        'AddressLine3' => $additionalAddress,
                        'City' => $order->deliveryAddress->town,
                        'State' => $order->deliveryAddress->state->isoCode ?: '',
                        'Zip' => $order->deliveryAddress->postalCode,
                        'Country' => $order->deliveryAddress->country->isoCode2,
                        'Phone' => (!empty(trim($order->deliveryAddress->phone)) ? $order->deliveryAddress->phone : '0000000000'),
                        'Email' => $order->deliveryAddress->email,
                        'Vat' => $order->deliveryAddress->taxIdNumber,
                    ),
                    'Length' => $packageType->length * $dimRate,
                    'Width' => $packageType->width * $dimRate,
                    'Height' => $packageType->height * $dimRate,
                    'Weight' => $package->weight / 1000,
                    'WeightUnit' => 'kg',
                    'DimUnit' => 'cm',
                    'Value' => number_format($this->totalPrice - 0.0001, 2, '.', ''),
                    'Currency' => $this->currency,
                    'DeclarationType' => $this->config->get('SpringGDS.declarationType'),
                    'Products' => $this->items,
                    'Source' => 'plentymarkets',
                ),
            )
        );
    }

    /**
     * Retrieves the label file from a given URL and saves it in S3 storage
     *
     * @param $labelUrl
     * @param $key
     * @return StorageObject
     */
    private function saveLabelToS3($labelUrl, $key)
    {
        $ch = curl_init();
        // Set URL to download
        curl_setopt($ch, CURLOPT_URL, $labelUrl);
        // Include header in result? (0 = yes, 1 = no)
        curl_setopt($ch, CURLOPT_HEADER, 0);
        // Should cURL return or print out the data? (true = return, false = print)
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Timeout in seconds
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        // Download the given URL, and return output
        $output = curl_exec($ch);
        // Close the cURL resource, and free system resources
        curl_close($ch);

        return $this->storageRepository->uploadObject('SpringGDS', $key, $output);
    }

    /**
     * Saves the shipping information
     *
     * @param int $orderId
     * @param $shipmentDate
     * @param array $shipmentItems
     */
    private function saveShippingInformation(int $orderId, $shipmentDate, array $shipmentItems): void
    {
        $transactionIds = array();

        foreach ($shipmentItems as $shipmentItem) {
            $transactionIds[] = $shipmentItem['shipmentNumber'];
        }

        /** DateTime::W3C removed in PHP 7.2 */
        $shipmentAt = date('Y-m-d\TH:i:sP', strtotime($shipmentDate));
        $registrationAt = date('Y-m-d\TH:i:sP');

        $data = [
            'orderId' => $orderId,
            'transactionId' => implode(',', $transactionIds),
            'shippingServiceProvider' => 'SpringGDS',
            'shippingStatus' => 'registered',
            'shippingCosts' => 0.00,
            'additionalData' => $shipmentItems,
            'registrationAt' => $registrationAt,
            'shipmentAt' => $shipmentAt
        ];

        $this->shippingInformationRepositoryContract->saveShippingInformation($data);
    }

    /**
     * Returns all order ids with shipping status 'open'
     *
     * @param array $orderIds
     * @return array
     */
    private function getOpenOrderIds(array $orderIds): array
    {
        $openOrderIds = array();

        foreach ($orderIds as $orderId) {
            $shippingInformation = $this->shippingInformationRepositoryContract->getShippingInformationByOrderId($orderId);

            if ($shippingInformation->shippingStatus === null || $shippingInformation->shippingStatus === 'open') {
                $openOrderIds[] = $orderId;
            }
        }

        return $openOrderIds;
    }

    /**
     * Returns all order ids from request object
     *
     * @param Request $request
     * @param $orderIds
     * @return array
     */
    private function getOrderIds(Request $request, $orderIds): array
    {
        if (is_numeric($orderIds)) {
            $orderIds = array($orderIds);
        } elseif (!is_array($orderIds)) {
            $orderIds = $request->get('orderIds');
        }

        return $orderIds;
    }

    /**
     * Handling of response values, fires S3 storage and updates order shipping package
     *
     * @param string $labelUrl
     * @param string $shipmentNumber
     * @param string $sequenceNumber
     * @return array
     */
    private function handleAfterRegisterShipment($labelUrl, $shipmentNumber, $sequenceNumber): array
    {
        $shipmentItems = array();
        $storageObject = $this->saveLabelToS3(
            $labelUrl,
            $shipmentNumber . '.pdf'
        );

        $shipmentItems[] = array(
            'labelUrl' => $labelUrl,
            'shipmentNumber' => $shipmentNumber,
        );

        $this->orderShippingPackage->updateOrderShippingPackage(
            $sequenceNumber,
            [
                'packageNumber' => $shipmentNumber,
                'label' => $storageObject->key
            ]
        );

        return $shipmentItems;
    }

    /**
     * @param string $data
     * @return object
     */
    public function curlApi(string $data): object
    {
        $ch = curl_init(
            'https://mtapi.net/'
            . ($this->config->get('SpringGDS.mode', '0') === '0' ? '?testMode=1' : '')
        );
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = json_decode(curl_exec($ch));
        curl_close($ch);

        return $response;
    }

    /**
     * @param string $trackingNumber
     * @return string
     */
    public function genLabelUrl(string $trackingNumber = ''): string
    {
        return 'https://mailingtechnology.com/API/label_plenty.php?testMode='
            . (int)!$this->config->get('SpringGDS.mode', '0')
            . '&userId=api&tn=' . $trackingNumber
            . '&format=' . $this->config->get('SpringGDS.format', 'PDF')
            . '&apikey=' . $this->config->get('SpringGDS.apiKey', '');
    }

    /**
     * @param array $arr
     * @return string
     */
    protected function concatNotEmpty(array $arr): string
    {
        return implode(' ', array_filter($arr, function ($value) {
            return !is_null(trim($value)) && trim($value) !== '';
        }));
    }
}
