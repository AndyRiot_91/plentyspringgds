<?php

namespace SpringGDS\Migrations;

use Plenty\Modules\Order\Shipping\ServiceProvider\Contracts\ShippingServiceProviderRepositoryContract;
use Plenty\Plugin\Log\Loggable;

/**
 * Class CreateShippingServiceProviderSpringGDS
 * @package SpringGDS\Migrations
 */
class CreateShippingServiceProvider
{
    use Loggable;

    /** @var ShippingServiceProviderRepositoryContract $shippingServiceProviderRepository */
    private $shippingServiceProviderRepository;

    /** @param ShippingServiceProviderRepositoryContract $shippingServiceProviderRepository */
    public function __construct(ShippingServiceProviderRepositoryContract $shippingServiceProviderRepository)
    {
        $this->shippingServiceProviderRepository = $shippingServiceProviderRepository;
    }

    /** @return void */
    public function run(): void
    {
        try {
            $this->shippingServiceProviderRepository->saveShippingServiceProvider(
                'SpringGDS',
                'SpringGDS');
            $this->getLogger('SpringGDS')
                ->info('SpringGDS shipping service provider successfully added');
        } catch (\Exception $e) {
            $this->getLogger('SpringGDS')
                ->critical(
                    'Could not save or update shipping service provider SpringGDS. Code ' . $e->getCode()
                    . ': ' . $e->getMessage()
                );
        }
    }

}
