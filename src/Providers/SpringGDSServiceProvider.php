<?php

namespace SpringGDS\Providers;

use Plenty\Modules\Order\Shipping\ServiceProvider\Services\ShippingServiceProviderService;
use Plenty\Plugin\ServiceProvider;

/**
 * Class SpringGDSServiceProvider
 * @package SpringGDS\Providers
 */
class SpringGDSServiceProvider extends ServiceProvider
{
    /** Register the service provider. */
    public function register(): void
    {
        // add REST routes by registering a RouteServiceProvider if necessary
//        $this->getApplication()->register(SpringGDSRouteServiceProvider::class);
    }

    /**
     * @param ShippingServiceProviderService $shippingServiceProviderService
     */
    public function boot(ShippingServiceProviderService $shippingServiceProviderService): void
    {
        $shippingServiceProviderService->registerShippingProvider(
            'SpringGDS',
            [
                'de' => 'SpringGDS',
                'en' => 'SpringGDS'
            ],
            [
                'SpringGDS\\Controllers\\SpringGDSController@registerShipments',
                'SpringGDS\\Controllers\\SpringGDSController@getLabels',
                'SpringGDS\\Controllers\\SpringGDSController@deleteShipments',
            ]
        );
    }

}
