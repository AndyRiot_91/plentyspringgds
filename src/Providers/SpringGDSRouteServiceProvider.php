<?php

namespace SpringGDS\Providers;

use Plenty\Plugin\RouteServiceProvider;
use Plenty\Plugin\Routing\Router;

/**
 * Class SpringGDSRouteServiceProvider
 * @package SpringGDS\Providers
 */
class SpringGDSRouteServiceProvider extends RouteServiceProvider
{
    /**
     * @param Router $router
     */
    public function map(Router $router): void
    {
    }

}
