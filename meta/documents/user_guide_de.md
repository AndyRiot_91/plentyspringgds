# User Guide Spring Global Delivery Solutions - Shipping (Versand)

Dieses Plugin gibt dir Zugriff auf die vielfältigen Versandoptionen von Spring Global Delivery Solutions (Spring GDS) für den weltweiten Versand von Päckchen und Paketen.
Generiere Versandetiketten und erhalte Tracking-Updates für deine Sendungen.

Alle Informationen zu unserer Dienstleistung: Schau dir das plentymarkets Webinar an!

Webinar <a href="https://drive.google.com/file/d/1pR4mZUxwicqIZAuD25WufGg1mo9sgh3M/view?usp=sharing" target="_blank">Video</a> & <a href="https://attendee.gotowebinar.com/recording/6112484613641629704" target="_blank">Link</a>

## Bei Spring GDS anmelden

Um das Plugin nutzen zu können, musst du dich bei <a href="https://www.spring-gds.com/de/" target="_blank">Spring GDS</a> registrieren. Es wird ein API-Schlüssel bereitgestellt, mit dem du das Plugin in plentymarkets einrichten kannst.

## Einrichten von Spring GDS Shipping (Versand) in plentymarkets

### Spring GDS-API-Schlüssel konfigurieren

  * Installiere das Spring GDS Plugin unter **Plugins » Plugin-Übersicht**.
  * Klicke nach der Installation auf das Spring GDS Plugin und gib unter **Konfiguration » Global** den von Spring GDS bereitgestellten API-Schlüssel ein.
  * Um Labels zu generieren, die du für den Versand verwenden kannst, stelle bitte sicher, dass das Plugin im **Modus - Produktiv** ist. **Modus - Test** dient nur zum Testen der IT-Integration.

### Versandeinstellungen einrichten

Die folgenden Schritte stellen sicher, dass deine plentymarkets Umgebung für die Arbeit mit dem Spring GDS Shipping-Plugin eingerichtet ist. Alle Einstellungen findest du unter **Einrichtung » Aufträge » Versand » Optionen**.
  * <a href="https://knowledge.plentymarkets.com/slp/fulfilment/preparing-the-shipment#100" target="_blank">Lieferländer</a> einrichten.
  * <a href="https://knowledge.plentymarkets.com/slp/fulfilment/preparing-the-shipment#400" target="_blank">Regionen</a> erstellen.
  * Den <a href="https://knowledge.plentymarkets.com/slp/fulfilment/preparing-the-shipment#800" target="_blank">Versanddienstleister</a> **Spring GDS** erstellen.
   Verwende die **Tracking URL**: https://mailingtechnology.com/tracking/?tn=[PaketNr]
  * <a href="https://knowledge.plentymarkets.com/slp/fulfilment/preparing-the-shipment#1000" target="_blank">Versandprofile</a> und <a href="https://knowledge.plentymarkets.com/slp/fulfilment/preparing-the-shipment#1500" target="_blank">Portotabellen</a> für **Spring GDS** erstellen.
  * Verknüpfe die Spring GDS Services mit **Versandprofilen** und **Regionen** unter **Einrichtung » Aufträge » Versand » Spring GDS**
  
### Aufträge bei Spring GDS im Versand-Center registrieren
Aufträge können bei Spring GDS registriert werden über das **Aufträge » Versand-Center**.
  * Suche auf der Registerkarte **Suche** die Aufträge, die du mit Spring GDS versenden möchtest, und wähle sie aus.
  * Wähle auf der Registerkarte **Anmelden** den **Versanddienstleister** - **Spring GDS** aus und klicke auf das Symbol **Anmelden**.
  * Dadurch wird ein Versandetikett für die Aufträge erstellt und die **Paketnummer** mit dem Tracking-Link festgelegt.