# Release Notes for Spring GDS Versand

## v1.1.8 (2021-12-08)
 
### Added
- SpringClear Service

## v1.1.7 (2021-08-03)
 
### Fixed
- Wir verwenden $order->shippingProfileId in Controllers/SpringGDSController.php

## v1.1.6 (2021-07-07)
 
### Fixed
- Version 1.1.5 nicht von Plentymarkets erhalten

## v1.1.5 (2021-05-18)
 
### Fixed
- Einrichtung - Aufträge - Versand - Spring GDS zeigte nur Regionen an, die für das erste Versandprofil konfiguriert wurden

## v1.1.4 (2020-08-28)
 
### Fixed
- Bundle-Informationen für das Zollformular

### Added
- Link zum Webinar

## v1.1.3 (2020-04-30)
 
### Fixed
- Adresszusatz auf dem Etikett

## v1.1.2 (2020-04-14)
 
### Fixed
- User Guide

## v1.1.1 (2020-04-08)
 
### Fixed
- User Guide

## v1.1.0 (2020-03-27)
 
### Added
- Auswahl des Spring GDS-Service für Versandprofile und Regionen
- Fremdwährungsunterstützung für Versandetiketten

## v1.0.2 (2020-02-21)
 
### Added
- Möglichkeit: Referenz auf dem Etikett
 
### Fixed
- Produktwertfehler für Nicht-EU-Ziele

## v1.0.1 (2020-02-06)
 
### Fixed
- User Guide
 
## v1.0.0 (2020-01-24)
 
### Added
- Ursprüngliche Plugin-Version