# Release Notes for Spring GDS Versand

## v1.1.8 (2021-12-08)
 
### Added
- SpringClear service

## v1.1.7 (2021-08-03)
 
### Fixed
- We use $order->shippingProfileId in Controllers/SpringGDSController.php

## v1.1.6 (2021-07-07)
 
### Fixed
- Version 1.1.5 not received by Plentymarkets

## v1.1.5 (2021-05-18)
 
### Fixed
- Setup - Orders - Shipping - Spring GDS showed only regions configured for the first shipping profile

## v1.1.4 (2020-08-28)
 
### Fixed
- Use bundle information for customs form

### Added
- Link to webinar

## v1.1.3 (2020-04-30)
 
### Fixed
- Additional address information on the label

## v1.1.2 (2020-04-14)
 
### Fixed
- User Guide

## v1.1.1 (2020-04-08)
 
### Fixed
- User Guide

## v1.1.0 (2020-03-27)
 
### Added
- Spring GDS service selection for shipping profiles and regions
- Foreign currency support for shipping labels

## v1.0.2 (2020-02-21)
 
### Added
- Option: Reference on label
 
### Fixed
- Product value error for non-EU destinations

## v1.0.1 (2020-02-06)
 
### Fixed
- User guide
 
## v1.0.0 (2020-01-24)
 
### Added
- Initial plugin version